import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function translateHttpLoader(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, '../assets/i18n/','.json');
}

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TranslateModule.forChild(
      {
        loader: { provide: TranslateLoader, useFactory: translateHttpLoader }
      }
    )
  ],exports:[
    TranslateModule
  ]
})
export class SharedModule 
{ 
    static forTranslateRoot(): ModuleWithProviders<TranslateModule> {
      return TranslateModule.forRoot({
        loader: {
          provide : TranslateLoader,
          useFactory: translateHttpLoader,
          deps: [HttpClient]
        }
      });
    }  
}
