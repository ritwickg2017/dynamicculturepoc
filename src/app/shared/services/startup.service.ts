import { HttpClient } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { tap } from 'rxjs/operators';
import { LanguageTranslateService } from './language-translate.service';

@Injectable({
  providedIn: 'root'
})
export class StartupService {

  private httpClient: HttpClient;
  private languageService: LanguageTranslateService;
  public dateFormat: string;

  constructor(private injector: Injector) { }

  getCultureData() {
    this.getDependencies();
    return () => {
      return this.httpClient.get('../../assets/defaultConfig.json').toPromise().then((data: any) => {
        this.dateFormat = data.dateFormat;
        if(data.defaultLanguage === 'en-US') {
              this.languageService.setLanguage('english');
            } else if (data.defaultLanguage === 'es-ES') {
              this.languageService.setLanguage('spain');
            } else {
              this.languageService.setLanguage('english');
            }
      });
    }
  }
  getDependencies() {
    this.httpClient = this.injector.get(HttpClient);
    this.languageService = this.injector.get(LanguageTranslateService);
  }
}
