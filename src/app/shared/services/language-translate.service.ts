import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class LanguageTranslateService {

  constructor(private translateService: TranslateService) { }

  setLanguage(language: string) {
    this.translateService.use(language);
  }
  instantTranslate(text: string) {
    this.translateService.instant(text);
  }
}
