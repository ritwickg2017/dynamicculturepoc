import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LanguageTranslateService } from '../shared/services/language-translate.service';
import { StartupService } from '../shared/services/startup.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public dob: string;
  constructor(public startupService: StartupService, private translateSvc: LanguageTranslateService) { }

  ngOnInit(): void {
    this.dob = '10/30/1996';
  }
  changeLanguage(): void {
    this.translateSvc.setLanguage('english');
  }

}
