import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { StartupService } from './shared/services/startup.service';
import { HomeModule } from './home/home.module';

export function initialiseApp(startupService: StartupService) {
  return startupService.getCultureData();
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    HomeModule,
    HttpClientModule,
    SharedModule.forTranslateRoot()
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initialiseApp ,
      deps : [StartupService],
      multi:true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
